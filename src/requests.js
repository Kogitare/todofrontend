const url = "http://localhost:3000"
const endpoint = "/api/v1/tasks/"

async function get(_id=null) {
    if (_id){
        //
    } else {
        try{
            const response = await fetch(url+endpoint, {
                method: "GET"
            });
            return response.json();
        } catch(err) {
            console.error(err);
        }
    }
}

async function put(_id, change) {
    if (_id && change){
        try{
            const response = await fetch(url+endpoint+_id, {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(change)
            });
            return response.json();
        } catch(err) {
            console.error(err);
        }
    } else {
        console.error("No _id or change specified");
    }
}

async function post(task) {
    if (task){
        try{
            const response = await fetch(url+endpoint, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(task)
            });
            return response.json();
        } catch(err) {
            console.error(err);
        }
    } else {
        console.error("No task specified");
    }
}

async function del(_id) {
    if (_id){
        try{
            await fetch(url+endpoint+_id, {
                method: "DELETE"
            });
        } catch(err) {
            console.error(err);
        }
    } else {
        console.error("No _id or change specified");
    }
}

export {get, put, post, del};