import './App.css';
import React from 'react';
import {get, put, post, del} from './requests';

import doneIcon from './icons/done.png'
import notDoneIcon from './icons/notDone.png'
import dontDeleteIcon from './icons/dontDelete.png'
import deleteIcon from './icons/delete.png'
import addIcon from './icons/plus.png'


// component that represents 1 task
class Task extends React.Component {
  constructor(props) {
    super(props);
    this.state = {done: this.props.done, to_delete: false};
    this.changeStatus = this.changeStatus.bind(this);
    this.remove = this.remove.bind(this);
  };

  // changes status (determines if task is done)
  changeStatus() {
    this.props.changeTask(this.props._id, !this.state.done);
    this.setState(prevState => ({
      done: !prevState.done
    }));
  };

  // remove
  remove() {
    this.props.deleteTask(this.props._id);
  };

  // render
  render() {
    return (
      <div className={"task"+(this.state.done ? " taskDone" : "")}>
        <p className="taskText">{this.props.name}</p>
        <button className={"taskButton firstTaskButton"} onClick={this.changeStatus}>
          <img className="icon" src={this.state.done ? doneIcon : notDoneIcon} alt="done" />
        </button>
        <button className="taskButton">
          <img className="icon" onClick={this.remove} src={!this.state.done ? deleteIcon : dontDeleteIcon} alt="done" />
        </button>
      </div>
    );
  };
};



// component responsible for input
class TaskInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''}

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // handle changing input box
  handleChange(event) {
    if (event.target.value.length <= 50) {
      this.setState({value: event.target.value});
    } else {
      this.setState({value: this.state.value});
    }
  };

  // handle submit (creating new task)
  handleSubmit(event) {
    this.props.addTask(this.state.value);
    event.preventDefault();
    this.setState({value: ''});
  }

  // render
  render() {
    return (
      <div className="task">
        <form className="form" onSubmit={this.handleSubmit}>
          <label>
            <input className="formText" type="text" placeholder="new task name" value={this.state.value} onChange={this.handleChange}/>
          </label>
          <input className="formSubmit" type="image" size={100} maxLength={50} src={addIcon} alt="Add"/>
        </form>
      </div>
    );
  }
}



// component with tasks and input
class Tasks extends React.Component {
  constructor(props) {
    super(props);

    // for updating things
    this.state = {changer: true};
    this.tasks = [];

    // to allow stuff
    this.componentDidMount = this.componentDidMount.bind(this);
    this.showCurrentTasks = this.showCurrentTasks.bind(this);
    this.getCurrentTasks = this.getCurrentTasks.bind(this);
    this.changeTask = this.changeTask.bind(this);
    this.deleteTask = this.deleteTask.bind(this);
    this.addTask = this.addTask.bind(this);
  }

  // get tasks when first mounted
  componentDidMount() {
    this.getCurrentTasks();
  };

  // show current tasks
  showCurrentTasks() {
    return (this.tasks.map(val => {
      return <Task name={val.name} done={val.done} changeTask={this.changeTask} deleteTask={this.deleteTask} _id={val._id} key={val._id}/>
    }));
  }

  // get current tasks
  getCurrentTasks() {
    get()
      .then((data) => {
        this.tasks = data.tasks.slice();
        const doneTasks = this.tasks.filter(val => val.done === true);
        const undoneTasks = this.tasks.filter(val => val.done === false);
        this.tasks = undoneTasks.concat(doneTasks);
        this.setState(prevState => ({
          changer: !prevState.changer
        }));
      })
      .catch((err) => {
        console.error(err);
      });
  };

  // change task
  changeTask(_id, done) {
    put(_id, {done})
      .then((id) => {
        this.getCurrentTasks();
      });
  };

  // add task
  addTask(name) {
    post({name})
      .then((id) => {
        this.getCurrentTasks();
      });
  };

  // remove task
  deleteTask(_id) {
    del(_id)
      .then((id) => {
        this.getCurrentTasks();
      });
  };

  // render
  render() {
    return (
      <div className="tasks">
        <TaskInput addTask={this.addTask}/>
        {this.showCurrentTasks()}
      </div>
    );
  };
}



// App component
class App extends React.Component {
  render() {
    return (
      <div className="App">
        <h1>to do list</h1>
        <Tasks/>
      </div>
    );
  };
};

export default App;